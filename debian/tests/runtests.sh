#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

check_files_exist() {

        for f in "$@"; do
	        if [ ! -e $f ]; then
	                echo "ERROR: $f does not exist" >&2
	                exit 1
	        else
	        echo "OK: $f is there"
	        fi
        done
}

check_data_exists() {

        result=$(/usr/bin/sqlite3 ~/.recon-ng/keys.db .dump | grep "'google_api','12345'"| wc -l)
        if [ $result -eq "0" ]; then
                echo "ERROR: data does not exist"
                exit 1
        else
                echo "OK: data is there"        
        fi        

}

run_help() {

        /usr/bin/recon-cli -h
	/usr/bin/recon-ng -h
	/usr/bin/recon-rpc -h

}

test_recon-ng() {

    cat <<EOF > $AUTOPKGTEST_TMP/recon_ng_help
#!/usr/bin/expect
spawn /usr/bin/recon-ng
expect "\[recon-ng\]\[default\] >"
send "help\n"
expect "\[recon-ng\]\[default\] >"
send "show modules\n"
expect "\[recon-ng\]\[default\] >"
send "keys list\n"
expect "\[recon-ng\]\[default\] >"
send "keys add google_api 12345\n"
expect "\[recon-ng\]\[default\] >"
send "keys list\n"
expect "\[recon-ng\]\[default\] >"
send "workspaces list\n"
expect "\[recon-ng\]\[default\] >"
send "workspaces add test\n"
expect "\[recon-ng\]\[default\] >"
send "shell ls -l  ~/.recon-ng/workspaces/\n"
expect "\[recon-ng\]\[default\] >"
send "workspaces select test\n"
expect "\[recon-ng\]\[test\] >"
send "add domains\n"
expect "domain \(TEXT\):"
send "kali.org\n"
expect "\[recon-ng\]\[test\] >"
send "query SELECT * FROM domains\n"
expect "\[recon-ng\]\[test\] > "
send "use recon/domains-hosts/bing_domain_web\n"
expect "\[recon-ng\]\[test\]\[bing_domain_web\] >"
send "run\n"
expect "\[recon-ng\]\[test\]\[bing_domain_web\] >"
send "query SELECT * FROM hosts\n"
expect "\[recon-ng\]\[test\]\[bing_domain_web\] >"
send "back\n"
expect "\[recon-ng\]\[test\] >"
send "search xss\n"
expect "\[recon-ng\]\[test\] >"
send "use recon/domains-vulnerabilities/xssed\n"
expect "\[recon-ng\]\[test\]\[xssposed\] >"
send "show info\n"
expect "\[recon-ng\]\[test\]\[xssposed\] >"
send "set SOURCE kali.org\n"
expect "\[recon-ng\]\[test\]\[xssposed\] >"
send "run\n"
expect "\[recon-ng\]\[test\]\[xssposed\] >"
send "back\n"
expect "\[recon-ng\]\[test\] >"
send "exit\n"
EOF

        chmod 755 $AUTOPKGTEST_TMP/recon_ng_help
        $AUTOPKGTEST_TMP/recon_ng_help
        check_files_exist ~/.recon-ng/workspaces/test/data.db
        check_data_exists
        rm -rf  ~/.recon-ng/workspaces/test

}


test_reconngrpc() {


        screen -S reconngrpctest -d -m recon-rpc
        COUNTER=0
        while [[ $(netstat -ltn | grep ":4141" | wc -l) -eq "0" ]] ; do
                if  [ "$COUNTER" -gt 60 ]; then
                        echo "NO! service recon-rpc still not running after 60 seconds ! Error..."
                        exit 1
                fi
                echo "Testing whether recon-rpc is listening on port 4141..."
                sleep 1
                let COUNTER=COUNTER+1
        done
        echo "OK! service recon-rpc is now listening on port 4141."

        screen -X -S reconngrpctest quit
}


###################################
# Main
###################################

for function in "$@"; do
        $function
done
